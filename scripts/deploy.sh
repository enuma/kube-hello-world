export DEPLOYMENTS_PATH=../kubernetes-files/deployments
export SERVICES_PATH=../kubernetes-files/services
export CONTROLLERS_PATH=../kubernetes-files/controllers
export INGRESS_PATH=../kubernetes-files/ingress-rules

export INGRESS_NAMESPACE=ingress

bash kube-scripts/deploy-demo-services.sh
bash kube-scripts/deploy-ingress.sh