kubectl create -f $DEPLOYMENTS_PATH/deployment-1.yaml -f $SERVICES_PATH/service-1.yaml

## expose deployment to test it's working
# kubectl expose deployment deployment-1 --type=NodePort
## get service url
# minikube service deployment-1 --url

# create deployment2 & service2
kubectl create -f $DEPLOYMENTS_PATH/deployment-2.yaml -f $SERVICES_PATH/service-2.yaml