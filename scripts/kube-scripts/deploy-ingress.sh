kubectl create namespace $INGRESS_NAMESPACE


# create default backend deployment & service
kubectl create -f $DEPLOYMENTS_PATH/default-backend-deployment.yaml -f $SERVICES_PATH/default-backend-service.yaml -n=$INGRESS_NAMESPACE

# create controller config
kubectl create -f $INGRESS_PATH/nginx-ingress-controller-config-map.yaml -n=$INGRESS_NAMESPACE

# create rbac rules
kubectl create -f $INGRESS_PATH/nginx-ingress-controller-roles.yaml -n=$INGRESS_NAMESPACE

# create ingress controller
kubectl create -f $DEPLOYMENTS_PATH/nginx-ingress-controller-deployment.yaml -n=$INGRESS_NAMESPACE


# create ingress rules
kubectl create -f $INGRESS_PATH/nginx-ingress.yaml -n=$INGRESS_NAMESPACE
kubectl create -f $INGRESS_PATH/app-ingress.yaml


# expose ingress controller
kubectl create -f $SERVICES_PATH/nginx-ingress-controller-service.yaml -n=$INGRESS_NAMESPACE