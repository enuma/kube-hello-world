ANSIBLE_FILES_PATH=../ansible-files

ansible-playbook -v -i $ANSIBLE_FILES_PATH/hosts.yaml  $ANSIBLE_FILES_PATH/0-init.yaml
ansible-playbook -v -i $ANSIBLE_FILES_PATH/hosts.yaml  $ANSIBLE_FILES_PATH/1-kube-deps.yaml
ansible-playbook -v -i $ANSIBLE_FILES_PATH/hosts.yaml  $ANSIBLE_FILES_PATH/2-master.yaml
ansible-playbook -v -i $ANSIBLE_FILES_PATH/hosts.yaml  $ANSIBLE_FILES_PATH/3-workers.yaml